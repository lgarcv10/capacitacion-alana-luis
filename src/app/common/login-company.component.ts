import { Component, Input, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'login-company-component',
  templateUrl: './login-company.component.html',
  styleUrls: ['./login.component.css']
  
})
export class LoginCompanyComponent  { 

	@Input() public title_company: any;
  @Input() public subtitle_company: any;
  @Output() pasaDatosCo = new EventEmitter();

  public correo: string;
  public password: string;
  

  profiles = [
    {id: '0', name: 'Compañía'},
    {id: '1', name: 'Candidato'}
  ];


  // Cuando se lance el evento click en la plantilla llamaremos a este método
  throw(event: any){
  // Usamos el método emit
    this.pasaDatosCo.emit({correo: this.correo, password: this.password});
  }






}
