import { Component, Injectable, OnInit } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from './model/User.component';

@Injectable()
export class JobService {
    
    public requestURL: string;

    constructor(private http: Http){
        this.requestURL = 'https://apidev.alanajobs.com/job-position/index';
    }

    public getPositions(): Observable<any> {

        console.log(this.requestURL);
        return this.http.get(this.requestURL, this.jwt)
            .map(res => res.json()['response']); 
             
    };


    private jwt(): RequestOptions {
        // create authorization header with jwt token
        console.log('CURRENT JWT ====> '+localStorage.getItem('currentUser'));
        let currentManger = JSON.parse(localStorage.getItem('currentUser'));
        if (currentManger && currentManger.authToken) {
            let _headers = new Headers({ 'Authorization': 'Bearer ' + currentManger.authToken });
            return new RequestOptions({ headers: _headers });
        }
    }

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 


}