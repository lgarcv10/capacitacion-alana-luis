import { Injectable } from '@angular/core';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from './model/User.component';

@Injectable()
export class AuthenticationService {
    
    public requestURL: string;

    constructor(public http: Http){
        this.requestURL = 'https://apidev.alanajobs.com/secure-candidate/login_check';
    }

    //Method to Sign in users list
    public postSignIn(usuario: User): Observable<any> {

       // console.log('Datos Servicio, user: '+usuario.email+ ', password: '+usuario.password);
        
        let body: string = 'email='+ usuario.email + '&password=' + usuario.password;

        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let options = new RequestOptions({ headers: headers });

        console.log(this.requestURL);
        
        return this.http.post(this.requestURL, body, {headers: headers})
            .map((response: Response) => {
                console.log(response);
                response.json();
                localStorage.setItem('currentUser', JSON.stringify(usuario));
                localStorage.getItem('currentUser');
            })
            .catch(this.handleErrorObservable);
    };

    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    } 




}