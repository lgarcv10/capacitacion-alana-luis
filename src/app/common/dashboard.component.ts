import { Component, Input, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import { JobService } from './job.service';
import { User } from './model/User.component';
import { Position } from './model/Position.component';
import { Headers, Response, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise'; 


@Component({
  selector: 'dashboard-component',
  templateUrl:'./dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ JobService ]
  
})
export class DashboardComponent  { 

    constructor (private jobService: JobService, private http: Http){
        this.mostrarCorreo();
        this.getPositions();
    }

    private title_dashboard: string;
    public email: string = '';
    public password: string = '';
    public userData: any;
    public positionData: any;
    public usuario_: User = new User('','','');
    public positions: Position[];
    public position: Position;

    ngOnInit(){
        //Intento 1
       /* this.http.get('https://apidev.alanajobs.com/job-position/index').
        subscribe(data => {
            console.log('DATA STRING JSON ===> '+ (JSON.stringify(data.json())));
            this.positions = data.json()['response'];
        });*/

    }

    public seePositions(){
        this.positions.forEach((item, index) => {
        
        console.log(item); // 9, 2, 5
        console.log(index); // 0, 1, 2
    });
    }

    public toUser(data: string): User {
        let jsonData = JSON.parse(data);  
        this.userData = {token: jsonData.token, email: jsonData.email, password: jsonData.password};

        return this.userData;
    }

    public toPosition(data: string): User {
        let jsonData = JSON.parse(data);  
        this.positionData = { id: jsonData.id, jobPositionName: jsonData.jobPositionName };

        return this.positionData;
    }
  
    public mostrarCorreo(){
        //console.log('JSON A CONVERTIR =====>'+ localStorage.getItem('currentUser'));
        this.usuario_ = this.toUser(localStorage.getItem('currentUser'));
        this.title_dashboard = this.usuario_.email;
    }
      
    public getPositions(): void {

        this.jobService.getPositions().subscribe(
            positions => {
            this.positions = positions;
            //console.log('DATA STRING JSON ===> '+ (JSON.stringify(data.json())));
            }, (error) => {
            console.log(error);
            });
    }



    
}















