import { Component, Input, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import { AuthenticationService } from './authentication.service';
import { User } from './model/User.component';

@Component({
  selector: 'login-candidate-component',
  templateUrl: './login-candidate.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ AuthenticationService ]
  
})
export class LoginCandidateComponent  { 

    constructor (private authenticationService: AuthenticationService){

    }

    @Input() public title_candidate: any;
    @Input() public subtitle_candidate: any;
    @Output() pasaDatosCa = new EventEmitter();

    public username: string = 'test@alana.com';
    public password: string = 'Asd.1234';
    
    profiles = [
    {id: '0', name: 'Compañía'},
    {id: '1', name: 'Candidato'}
    ];

    public usuario: User= new User('','','');
  
 

    // Cuando se lance el evento click en la plantilla llamaremos a este método
    throw(event: any){
      // Usamos el método emit
        this.pasaDatosCa.emit({correo: this.username, password: this.password});
      }

    public signIn(): void {
      if (this.username != '' && this.password != '') {

        this.usuario.email = this.username;
        this.usuario.password = this.password;
          this.authenticationService.postSignIn(this.usuario).subscribe(
              success => {
                  //console.log('Sesion iniciada');
              },
              err => {
                console.log(err);
              });
      }
  }

    


    
}
