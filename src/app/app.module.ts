import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent }  from './app.component';
import { LoginComponent }  from './common/login.component';
import { LoginCandidateComponent }  from './common/login-candidate.component';
import { LoginCompanyComponent }  from './common/login-company.component';
import { DashboardComponent }  from './common/dashboard.component';
// Importar objetos de la librería http
import { Http, HttpModule, Response, RequestOptions, Headers } from '@angular/http';  
// Importar la clase Observable desde la librería rxjs
import { Observable }     from 'rxjs/Observable';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'login-candidate', component: LoginCandidateComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full'}
];


@NgModule({
  imports:[ 
    RouterModule.forRoot(appRoutes),
    BrowserModule, FormsModule, HttpModule ],
    declarations: [ AppComponent,
    LoginComponent,
    LoginCandidateComponent,
    LoginCompanyComponent,
    DashboardComponent],
  bootstrap:    [ AppComponent ],
  providers: [
    
  ]
})
export class AppModule { 

  /**
  * Constructor que reclama dependencias inyectables
  * Http se encuentra por haberse registrado en este módulo o en uno superior
  **/
  constructor(private http: Http) {
    // en el constructor no debe contener lógica extra
    // su función es únicamente recibir las dependencias
  }


 }
